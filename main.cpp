#include <iostream>
#include <omp.h>


using namespace std;


int partition(int numbers[], int p, int r)  {
    int pivot = numbers[p];
    int i = p, j = r, w;

    while (true) {
        while (numbers[j] > pivot)
            j--;

        while (numbers[i] < pivot)
            i++;

        if (i < j)
        {
            w = numbers[i];
            numbers[i] = numbers[j];
            numbers[j] = w;
            i++;
            j--;
        }
        else
            return j;
    }
}

void quicksort_parallel(int numbers[], int p, int r)
{
    int q;
    if (p < r)
    {
        q = partition(numbers,p,r);
#pragma omp task
        quicksort_parallel(numbers, p, q);
#pragma omp task
        quicksort_parallel(numbers, q+1, r);
    }
}

void quicksort_one_thread(int numbers[], int p, int r)
{
    int q;
    if (p < r)
    {
        q = partition(numbers,p,r);
        quicksort_one_thread(numbers, p, q);
        quicksort_one_thread(numbers, q+1, r);
    }
}

int main()
{
    int numberOfElements = 10000000;

    auto *numbers = new int[numberOfElements];

    for (int i = 0; i<numberOfElements; i++) {
        numbers[i] = rand() % numberOfElements;
    }

    double start_time = omp_get_wtime();
#pragma omp parallel
    {
#pragma omp single nowait
        quicksort_parallel(numbers, 0, numberOfElements-1);
    }
    double time = omp_get_wtime() - start_time;

    cout << "parallel quicksort: " << time << endl;

    for (int i = 0; i<numberOfElements; i++) {
        numbers[i] = rand() % numberOfElements;
    }

    start_time = omp_get_wtime();
    quicksort_one_thread(numbers, 0, numberOfElements-1);
    time = omp_get_wtime() - start_time;

    cout << "single thread quicksort: " << time << endl;
    return 0;
}